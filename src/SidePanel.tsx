import React, { useMemo } from 'react';
import './SidePanel.scss';

import { Score } from './snake-core';
import { ReactComponent as ArrowsIcon } from './icons/arrows.svg';
import { ReactComponent as Spacebar } from './icons/spacebar.svg';
import { ReactComponent as GitLabIcon } from './icons/gitlab-icon-rgb.svg';

interface SidePanelProps {
  score?: Score;
}

const SidePanel: React.FC<SidePanelProps> = ({ score }) => {
  const scoreHistory = score?.history;

  const currentScore = score?.current || 0;
  const maximumScore = useMemo(() => scoreHistory?.reduce((acc, item) => Math.max(acc, item.score), 0) || 0, [
    scoreHistory,
  ]);

  return (
    <aside className="side-panel">
      <section>
        <h1>
          <span role="img" aria-label="snake">
            🐍
          </span>
          The Snake
        </h1>
        <hr />
      </section>
      <section>
        <p>Controls</p>
        <ul>
          <li>
            <Spacebar /> to play, pause, restart
          </li>
          <li>
            <ArrowsIcon /> to move
          </li>
        </ul>
        <p>What to eat?</p>
        <ul>
          <li className="side-panel__food side-panel--common">Common, gives 10 points</li>
          <li className="side-panel__food side-panel--special">Special, gives 30 points</li>
        </ul>
        <hr />
      </section>
      <section className="side-panel__score">
        <p>Score</p>
        <h2>This session: {currentScore}</h2>
        <span>Maximum on this device: {currentScore > maximumScore ? currentScore : maximumScore}</span>
        <p>Your history</p>
        <ul>
          {!scoreHistory?.length && (
            <li>
              <small>No history at all :(</small>
            </li>
          )}
          {scoreHistory?.map(item => (
            <li key={item.date}>
              {item.score} <small>{new Date(item.date).toLocaleString()}</small>
            </li>
          ))}
        </ul>
      </section>
      <section>
        <hr />
        <p>Interested in source?</p>
        <p className="side-panel__link">
          Here you go:
          <a href="https://www.gitlab.com/DatWolfL/reactive-snake" target="_blank" rel="noopener noreferrer">
            <GitLabIcon width="36" height="36" /> GitLab repo
          </a>
        </p>
        <p>Jan Klíma &copy; 2020</p>
      </section>
    </aside>
  );
};

export default SidePanel;
