import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { SnakeBodyPart, Directions, Snake, Position, Food, Score, FoodSound, Scene } from './types';
import { canvasCenter, horizontalRects, verticalRects } from './canvas';
import { GameStore, SPEED_CORRECTION } from './state';

export const DIRECTIONS: Directions = {
  UP: [0, -1],
  DOWN: [0, 1],
  LEFT: [-1, 0],
  RIGHT: [1, 0],
};

export function mapControls(key: string) {
  switch (key) {
    case 'ArrowUp':
    case 'w': {
      return DIRECTIONS.UP;
    }
    case 'ArrowDown':
    case 's': {
      return DIRECTIONS.DOWN;
    }
    case 'ArrowLeft':
    case 'a': {
      return DIRECTIONS.LEFT;
    }
    case 'ArrowRight':
    case 'd': {
      return DIRECTIONS.RIGHT;
    }
    default: {
      return null;
    }
  }
}

export function getRandomNumber(min: number, max: number) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

export function getRandomPosition(): Position {
  return [getRandomNumber(1, horizontalRects - 2), getRandomNumber(1, verticalRects - 2)];
}

export function generateSnakeBodyParts(count: number): SnakeBodyPart[] {
  return Array.from({ length: count }).map(() => ({ position: canvasCenter() }));
}

export function generateFood(count: number, snakeBody: SnakeBodyPart[] = []): Food[] {
  return Array.from({ length: count }).map(() => {
    const luck = parseFloat(Math.random().toFixed(2));

    let newPosition = getRandomPosition();

    // eslint-disable-next-line
    while (snakeBody.some(bodyPart => checkCollision(bodyPart.position, newPosition))) {
      newPosition = getRandomPosition();
    }

    if (luck > 0.9) {
      return {
        color: '#ffa700',
        position: newPosition,
        score: 30,
        sound: FoodSound.Special,
      };
    }

    return {
      color: '#a0d541',
      position: newPosition,
      score: 10,
      sound: FoodSound.Common,
    };
  });
}

export function checkCollision(a: Position, b: Position) {
  return a[0] === b[0] && a[1] === b[1];
}

export function foodDensity() {
  return Math.trunc((horizontalRects * verticalRects) / 1000) || 1;
}

export function preventCounterDirection(prev: Position, next: Position) {
  if (prev[0] === next[0] * -1 || prev[1] === next[1] * -1) {
    return prev;
  }

  return next;
}

export function moveSnake() {
  return function (source$: Observable<[number, Position, Snake]>) {
    return source$.pipe(
      map<[number, Position, Snake], Snake>(([_, direction, snake]) => {
        const oldHead = snake.body[0];
        const oldTail = snake.body.pop(); // eslint-disable-line

        let newHead: SnakeBodyPart = {
          position: [oldHead.position[0] + direction[0], oldHead.position[1] + direction[1]],
        };

        if (newHead.position[0] === horizontalRects) {
          newHead.position[0] = 0;
        }

        if (newHead.position[0] < 0) {
          newHead.position[0] = horizontalRects - 1;
        }

        if (newHead.position[1] === verticalRects) {
          newHead.position[1] = 0;
        }

        if (newHead.position[1] < 0) {
          newHead.position[1] = verticalRects - 1;
        }

        return {
          ...snake,
          body: [newHead, ...snake.body],
        };
      }),
    );
  };
}

export function snakeBodyCollide(gameState: GameStore) {
  return function (snake: Snake) {
    const snakeHead = snake.body[0];
    const snakeBody = snake.body.slice(1, snake.body.length);

    if (snakeBody.some(bodyPart => checkCollision(bodyPart.position, snakeHead.position))) {
      gameState.changeScene(Scene.Dead);
    }
  };
}

export function commitSuicide(gameState: GameStore) {
  return function () {
    gameState.changeScene(Scene.Dead);
    new Audio('assets/suicide.mp3').play();
  };
}

export function eatFood(gameState: GameStore) {
  return function ([snake, food, score, gameSpeed]: [Snake, Food[], Score, number]) {
    const snakeHead = snake.body[0];
    const snakeTail = snake.body[snake.body.length - 1];

    food.forEach((item, index) => {
      if (checkCollision(snakeHead.position, item.position)) {
        const eatenFood = food.splice(index, 1)[0];
        const newScore = score.current + eatenFood.score;

        new Audio(`/assets/${newScore === 420 ? 'swed' : eatenFood.sound}.mp3`).play();

        gameState.updateFood([...food, ...generateFood(1, snake.body)]);
        gameState.updateSnake({
          ...snake,
          body: [...snake.body, { position: [snakeTail.position[0], snakeTail.position[1]] }],
        });

        gameState.updateScore({ ...score, current: newScore });
        gameState.changeGameSpeed(Math.floor(gameSpeed * SPEED_CORRECTION));
      }
    });
  };
}

export function changeGameScene(gameState: GameStore) {
  return function (scene: Scene) {
    switch (scene) {
      case Scene.Initial:
      case Scene.Paused: {
        return gameState.changeScene(Scene.Running);
      }
      case Scene.Running: {
        return gameState.changeScene(Scene.Paused);
      }
      case Scene.Dead: {
        return gameState.resetGame();
      }
    }
  };
}

export function saveScoreHistory(gameState: GameStore) {
  return function (score: Score) {
    const newHistory = [{ date: new Date().toISOString(), score: score.current }, ...score.history];

    try {
      localStorage.setItem('scoreHistory', JSON.stringify(newHistory));
    } catch (e) {}

    setTimeout(() => {
      gameState.updateScore({
        current: score.current,
        history: newHistory,
      });
    });
  };
}
