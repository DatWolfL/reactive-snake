export type Position = [number, number];
export type ISO8601Date = string;

export interface SnakeBodyPart {
  position: Position;
}

export interface Snake {
  headColor: string;
  bodyColor: string;
  body: SnakeBodyPart[];
}

export enum FoodSound {
  Common = 'common-chew',
  Special = 'special-chew',
}

export interface Food {
  color: string;
  position: Position;
  score: number;
  sound: FoodSound;
}

export enum Scene {
  Initial,
  Running,
  Paused,
  Dead,
}

export interface ScoreHistory {
  date: ISO8601Date;
  score: number;
}

export interface Score {
  current: number;
  history: ScoreHistory[];
}

export interface GameState {
  snake: Snake;
  food: Food[];
  gameSpeed: number;
  scene: Scene;
  score: Score;
}

export interface Directions {
  UP: Position;
  DOWN: Position;
  LEFT: Position;
  RIGHT: Position;
}

export interface DrawRectangleOptions {
  x: number;
  y: number;
  w?: number;
  h?: number;
  color?: string;
}

export interface DrawTextOptions {
  x?: number;
  y?: number;
  text: string;
  size?: number;
  color?: string;
}
