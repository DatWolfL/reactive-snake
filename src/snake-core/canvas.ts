import { Position, GameState, DrawRectangleOptions, Scene, Snake, Food, DrawTextOptions } from './types';

export const canvas = document.createElement('canvas');
export const canvasContext = canvas.getContext('2d')!; // in this case context will be always available

export let scale = parseFloat(getComputedStyle(document.documentElement).fontSize);
export let horizontalRects = 0;
export let verticalRects = 0;

export function spawnCanvas() {
  const target = document.body.querySelector('.snake-space') as HTMLDivElement;

  const { width, height } = target.getBoundingClientRect();

  horizontalRects = Math.trunc(width / scale);
  verticalRects = Math.trunc(height / scale);

  canvas.width = horizontalRects * scale;
  canvas.height = verticalRects * scale;

  clearCanvas();

  target.appendChild(canvas);
}

export function canvasCenter(): Position {
  return [Math.trunc(horizontalRects / 2), Math.trunc(verticalRects / 2)];
}

export function clearCanvas() {
  drawRectangle({ x: 0, y: 0, w: horizontalRects, h: verticalRects });
}

export function drawRectangle(options: DrawRectangleOptions) {
  const { x, y, w = 1, h = 1 } = options;

  canvasContext.fillStyle = options.color || '#dadada';
  canvasContext.fillRect(x * scale, y * scale, w * scale, h * scale);
}

export function drawText(options: DrawTextOptions) {
  const fontSize = (options.size || 2) * scale;

  canvasContext.fillStyle = options.color || '#FFFFFF';
  canvasContext.font = `${fontSize}px DM Mono`;
  canvasContext.textAlign = 'center';
  canvasContext.textBaseline = 'middle';

  String(options.text)
    .split('\n')
    .forEach((line, index) => {
      canvasContext.fillText(
        line,
        (options.x || horizontalRects / 2) * scale,
        (options.y || verticalRects / 2) * scale + index * fontSize,
      );
    });
}

const welcomeMessages = ["I've heard you like to gamble?", 'Reptile is waiting.', 'Hello there!', "Let's play."];
const pauseMessges = ['-273.15 °C', 'PAUSED!', 'The time has frozen', 'ZzzZzz'];
const deadMessages = ['Nevermind', 'Better luck next time', 'GAME OVER!', 'Damn', "You've just died", '😱'];

function pickRandomMessage(messagesArray: string[]) {
  return messagesArray[Math.floor(Math.random() * messagesArray.length)];
}

export function renderScene(gameState: GameState) {
  clearCanvas();

  switch (gameState.scene) {
    case Scene.Initial: {
      drawText({ color: '#000000', text: `${pickRandomMessage(welcomeMessages)}\nPress "space" to start` });
      break;
    }
    case Scene.Running: {
      renderEntities(gameState.snake, gameState.food);
      break;
    }
    case Scene.Paused: {
      renderEntities(gameState.snake, gameState.food);
      drawRectangle({ x: 0, y: 0, w: horizontalRects, h: verticalRects, color: 'rgba(0, 0, 0, 0.5)' });
      drawText({ text: `${pickRandomMessage(pauseMessges)}\nPress "space" to continue` });
      break;
    }
    case Scene.Dead: {
      const { current } = gameState.score;

      renderEntities(gameState.snake, gameState.food);
      drawRectangle({ x: 0, y: 0, w: horizontalRects, h: verticalRects, color: 'rgba(0, 0, 0, 0.5)' });
      drawText({ text: `${pickRandomMessage(deadMessages)}\nYour score was: ${current}\n\nTo restart press "space"` });
      break;
    }
  }
}

export function renderEntities(snake: Snake, food: Food[]) {
  snake.body.forEach((bodyPart, index) => {
    drawRectangle({
      x: bodyPart.position[0],
      y: bodyPart.position[1],
      color: !index ? snake.headColor : snake.bodyColor,
    });
  });

  food.forEach(food => {
    drawRectangle({ x: food.position[0], y: food.position[1], color: food.color });
  });
}
