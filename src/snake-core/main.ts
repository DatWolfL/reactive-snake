import { fromEvent, empty, of, interval } from 'rxjs';
import { switchMap, map, filter, startWith, distinctUntilChanged, withLatestFrom, pluck, scan, tap, buffer, share } from 'rxjs/operators'; // prettier-ignore

import { spawnCanvas, renderScene } from './canvas';
import { GameStore } from './state';
import { mapControls, DIRECTIONS, moveSnake, eatFood, snakeBodyCollide, preventCounterDirection, commitSuicide, changeGameScene, saveScoreHistory } from './helpers'; // prettier-ignore
import { Position, Scene } from './types';

export function initializeGame() {
  spawnCanvas();
  const gameState = new GameStore();

  const timer$ = gameState.gameSpeed$.pipe(
    switchMap(time =>
      interval(time).pipe(
        withLatestFrom(gameState.scene$),
        switchMap(([tick, scene]) => (scene === Scene.Running ? of(tick) : empty())),
      ),
    ),
    share(),
  );

  const keydown$ = fromEvent<KeyboardEvent>(document, 'keydown').pipe(share());

  keydown$
    .pipe(
      filter(event => event.key.toLowerCase() === 'k'),
      withLatestFrom(gameState.scene$),
      filter(([_, scene]) => scene === Scene.Running),
    )
    .subscribe(commitSuicide(gameState));

  keydown$
    .pipe(
      pluck('key'),
      filter(key => key === ' '),
      withLatestFrom(gameState.scene$),
      pluck('1'),
    )
    .subscribe(changeGameScene(gameState));

  const direction$ = keydown$.pipe(
    buffer(timer$),
    map(events => events.pop()),
    filter((event: KeyboardEvent | undefined): event is KeyboardEvent => !!event),
    map(event => mapControls(event.key)),
    filter((position: Position | null): position is Position => !!position),
    startWith(DIRECTIONS.UP),
    scan(preventCounterDirection),
    distinctUntilChanged(),
  );

  gameState.scene$
    .pipe(
      filter(scene => scene === Scene.Dead),
      withLatestFrom(gameState.score$),
      pluck('1'),
    )
    .subscribe(saveScoreHistory(gameState));

  timer$
    .pipe(
      withLatestFrom(direction$, gameState.snake$),
      moveSnake(),
      tap(gameState.updateSnake.bind(gameState)),
      tap(snakeBodyCollide(gameState)),
      withLatestFrom(gameState.food$, gameState.score$, gameState.gameSpeed$),
      tap(eatFood(gameState)),
    )
    .subscribe();

  gameState.gameState$.subscribe(renderScene);

  return gameState;
}
