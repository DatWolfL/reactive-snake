import { BehaviorSubject, Subject, queueScheduler } from 'rxjs';
import { pluck, scan, distinctUntilKeyChanged, shareReplay } from 'rxjs/operators';

import { GameState, Scene, Snake, Food, Score } from './types';
import { generateSnakeBodyParts, generateFood, foodDensity } from './helpers';

export const MAX_SPEED = 16;
export const INITIAL_SPEED = 200;
export const SPEED_CORRECTION = 0.97;

export class GameStore {
  private state: BehaviorSubject<GameState>;
  private updateState = new Subject();

  constructor() {
    const initialState = this.createGameState();
    this.state = new BehaviorSubject<GameState>(initialState);
    this.updateState.pipe(scan<any>((acc, val) => ({ ...acc, ...val }), initialState)).subscribe(this.state);
  }

  private createGameState(overrideDefaults?: Partial<GameState>): GameState {
    let scoreHistory;

    try {
      scoreHistory = localStorage.getItem('scoreHistory');
    } catch (e) {}

    return {
      food: generateFood(foodDensity()),
      gameSpeed: INITIAL_SPEED,
      scene: Scene.Initial,
      score: {
        current: 0,
        history: scoreHistory ? JSON.parse(scoreHistory) : [],
      },
      snake: {
        body: generateSnakeBodyParts(5),
        bodyColor: 'blue',
        headColor: 'darkblue',
      },
      ...overrideDefaults,
    };
  }

  get gameState$() {
    return this.state.asObservable().pipe(shareReplay(1));
  }

  get gameSpeed$() {
    return this.state.pipe(distinctUntilKeyChanged('gameSpeed'), pluck('gameSpeed'), shareReplay(1));
  }

  get scene$() {
    return this.state.pipe(distinctUntilKeyChanged('scene'), pluck('scene'), shareReplay(1));
  }

  get snake$() {
    return this.state.pipe(distinctUntilKeyChanged('snake'), pluck('snake'), shareReplay(1));
  }

  get food$() {
    return this.state.pipe(distinctUntilKeyChanged('food'), pluck('food'), shareReplay(1));
  }

  get score$() {
    return this.state.pipe(distinctUntilKeyChanged('score'), pluck('score'), shareReplay(1, 0, queueScheduler));
  }

  changeGameSpeed(newGameSpeed: number) {
    this.updateState.next({ gameSpeed: newGameSpeed < MAX_SPEED ? MAX_SPEED : newGameSpeed });
  }

  changeScene(newScene: Scene) {
    console.warn('Changing scene', newScene);
    this.updateState.next({ scene: newScene });
  }

  updateSnake(newSnakeState: Snake) {
    this.updateState.next({ snake: newSnakeState });
  }

  updateFood(newFood: Food[]) {
    this.updateState.next({ food: newFood });
  }

  updateScore(newScore: Score) {
    this.updateState.next({ score: newScore });
  }

  resetGame() {
    this.updateState.next(this.createGameState({ scene: Scene.Running }));
  }
}
