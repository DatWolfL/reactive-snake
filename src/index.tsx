import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import './styles.scss';

import { initializeGame, Score } from './snake-core';

import SidePanel from './SidePanel';
import SnakeSpace from './SnakeSpace';

const App = () => {
  const [score, setScore] = useState<Score>();

  useEffect(() => {
    (document as any).fonts.ready.then(() => {
      const gameState = initializeGame();
      const sub = gameState.score$.subscribe(setScore);

      return () => {
        sub.unsubscribe();
      };
    });
  }, []);

  return (
    <React.Fragment>
      <SidePanel score={score} />
      <SnakeSpace />
    </React.Fragment>
  );
};

ReactDOM.render(<App />, document.querySelector('#root'));
